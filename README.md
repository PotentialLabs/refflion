# README #

Refflion is an open source WiFi IoT development board based on the popular Arduino platform. This board uses the low cost ESP8266 WiFi Module for Wireless connectivity.

We're currently building the accompanying Ecosystem and Shields for the Board. 

Please contact us on info@potentiallabs.com if you'd like a sneak peek at the Board meanwhile. The Board will be launched for pre-order in 1st week of May after rigorous testing.
